/** interfejs dla gry Quiz */
interface Quiz {

    int MIN_VALUE = 0; // minimalny zakres poszukiwan
    int MAX_VALUE = 1000; // maksymalny zakres poszukiwan

    /**
     * metoda rzuca wyjatek ParamTooLarge w przypadku gdy parametr wejsciowy jest wiekszy
     * od oczekiwanej zmiennej, w przeciwnym wypadku rzuca wyjatek ParamTooSmall.
     */
    void isCorrectValue(int value) throws Quiz.ParamTooLarge, Quiz.ParamTooSmall;

    class ParamTooLarge extends Exception {}
    class ParamTooSmall extends Exception {}
}

/**
 * nalezy zaimplementowac interfejs Quiz.
 */
class QuizImpl implements Quiz {

    private int digit;

    public QuizImpl() {
        this.digit = 254; // w kazdej chwili zmienna moze otrzymac inna wartosc!
    }

    @Override
    public void isCorrectValue(int value) throws Quiz.ParamTooLarge, Quiz.ParamTooSmall {

        if(value > digit) {
            throw new Quiz.ParamTooLarge();
        }else if(value < digit) {
            throw new Quiz.ParamTooSmall();
        }
    }
}

class Main {

    /**
     * metoda main powinna implementowac algorytm do jak najszybszego wyszukiwania wartosci
     * zmiennej digit z klasy QuizImpl (zakladamy ze programista nie zna zawartosci klasy QuizImpl).
     * Nalezy zalozyc, ze pole gigit w klasie QuizImpl moze w kazdej chwili ulec zmianie. Do wyszukiwania
     * odpowiedniej wartosci nalezy wykorzystywac tylko i wylacznie metode isCorrectValue - jesli metoda
     * przestanie rzucac wyjatki wowczas mamy pewnosc ze poszukiwana zmienna zostala odnaleziona.
     */
    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();
        int digit = Quiz.MAX_VALUE / 2;
        int temp = digit / 2;

        for(int counter = 1; ;counter++) {

            try {

                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: " + digit + " Ilosc prob: " + counter);
                break;

            } catch(Quiz.ParamTooLarge e) {

                System.out.println("Argument za duzy!!!");

                digit = digit - temp;
                if(temp > 1) {
                    temp = temp / 2;
                } else {
                    temp = 1;
                }

            } catch(Quiz.ParamTooSmall e) {

                System.out.println("Argument za maly!!!");

                digit = digit + temp;
                if(temp > 1) {
                    temp = temp / 2;
                } else {
                    temp = 1;
                }
            }
        }
    }
}